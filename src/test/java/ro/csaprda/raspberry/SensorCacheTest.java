package ro.csaprda.raspberry;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import ro.csaprda.raspberry.model.SensorRead;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SensorCacheTest {

    private static final double HUMIDITY = 12.3;
    private static final double TEMPERATURE = 12.5;
    private static final double SENSOR_HEIGHT = 1.5;
    private static final String SENSOR_2 = "sensor2";
    private static final String SENSOR_1 = "sensor1";
    private static SensorCache cache;
    private static File cacheFile = new File("cache.dat");

    private static SensorRead sensorReading1;
    private static SensorRead sensorReading2;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        cache = SensorCache.getInstance();
    }

    @After
    public void cleanUp() throws Exception {
        cache.removeSensorReadings(cache.getAllSensorReadings());
    }

    @Test
    public void testGetInstance() {
        assertEquals(cache, SensorCache.getInstance());
        assertTrue(cache.equals(SensorCache.getInstance()));
    }

    @Test
    public void testGetAll() {
        initializeCache();
        verifyCacheContainsSensor(SENSOR_1);
    }

    @Test
    public void testRemoveCollectionOfSensorRead() {
        initializeCache();
        verifyCacheContainsSensor(SENSOR_1);
        assertEquals(getObjectCountFromFile().size(), 2);
        cache.removeSensorReadings(createSensorReadingsArray());
        assertEquals(cache.getAllSensorReadings().size(), 0);
        assertEquals(getObjectCountFromFile().size(), 0);
    }

    @Test
    public void testRemoveSensorRead() {
        initializeCache();
        verifyCacheContainsSensor(SENSOR_1);
        cache.removeSensorReading(sensorReading1);
        assertEquals(cache.getAllSensorReadings().size(), 1);
        assertEquals(cache.getAllSensorReadings().iterator().next().getSensorId(), SENSOR_2);
        assertEquals(getObjectCountFromFile().size(), 1);
    }

    private void initializeCache() {
        initializeSensors();
        cache.putSensorReading(sensorReading1);
        cache.putSensorReading(sensorReading2);
    }

    private void verifyCacheContainsSensor(String sensor) {
        assertEquals(cache.getAllSensorReadings().size(), 2);
        assertEquals(cache.getAllSensorReadings().iterator().next().getSensorId(), sensor);
    }

    @Test
    public void testPutCollectionOfSensorRead() {
        assertEquals(getObjectCountFromFile().size(), 0);
        initializeSensors();
        Collection<SensorRead> collection = createSensorReadingsArray();
        cache.putSensorReadings(collection);
        assertEquals(cache.getAllSensorReadings().size(), 2);
        assertEquals(getObjectCountFromFile().size(), 2);
    }

    private void initializeSensors() {
        sensorReading1 = new SensorRead(SENSOR_1, new Date(), SENSOR_HEIGHT, true, TEMPERATURE, HUMIDITY);
        sensorReading2 = new SensorRead(SENSOR_2, new Date(), SENSOR_HEIGHT, true, TEMPERATURE, HUMIDITY);
    }

    private Collection<SensorRead> createSensorReadingsArray() {
        Collection<SensorRead> collection = new ArrayList<SensorRead>();
        collection.add(sensorReading1);
        collection.add(sensorReading2);
        return collection;
    }

    @SuppressWarnings("unchecked")
    private Collection<SensorRead> getObjectCountFromFile() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cacheFile));) {
            Collection<SensorRead> cacheFromFile = (Collection<SensorRead>) ois.readObject();
            return cacheFromFile;
        } catch (EOFException e) {
            return new ArrayList<SensorRead>();
            // do nothing
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return new ArrayList<SensorRead>();
        }
    }

}
