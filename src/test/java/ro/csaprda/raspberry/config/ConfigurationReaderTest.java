package ro.csaprda.raspberry.config;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import ro.csaprda.raspberry.util.JsonParserWrapper;
import ro.csaprda.raspberry.util.SystemVariables;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class ConfigurationReaderTest {

    private ConfigurationReader configurationReader;

    @Mock
    private Logger log;

    @Before
    public void setUpBeforeClass() throws Exception {
        MockitoAnnotations.initMocks(this);
        configurationReader = new ConfigurationReader();
        Whitebox.setInternalState(configurationReader, "log", log);
    }

    @Test
    public void testThatLogFileNameIsSetRegardlessOfSuccessfulConfigRead() {
        assertEquals(SystemVariables.getLogFile(), "application.log");
    }

    @SuppressWarnings("static-access")
    @Test
    public void readConfigurationFromFile_testSuccessfulRead() {
        boolean hasConfigBeenReadSuccessfully = configurationReader.readConfigurationFromFile();
        assertTrue(hasConfigBeenReadSuccessfully);
        assertEquals(SystemVariables.getCacheFile(), "cache.dat");
        assertEquals(SystemVariables.getLogFile(), "application.log");
        assertEquals(SystemVariables.getReadingsURL(), "http://siloz-zdeno.rhcloud.com/rest/reading/");
        assertEquals(SystemVariables.getReadInterval(), 1 * 60 * 60 * 1000);
        assertEquals(SystemVariables.getMailUsername(), "spamorama@mail.com");
        assertEquals(SystemVariables.getMailPassword(), "spamorama");
        assertEquals(SystemVariables.getMailFrom(), "spamorama@mail.com");
        assertEquals(SystemVariables.getMailTo().size(), 2);
        assertEquals(SystemVariables.getMailTo().iterator().next(), "c.zdeno@gmail.com");
        assertEquals(SystemVariables.getMailTo().toArray()[1], "ivovidius@yahoo.com");
        assertTrue(SystemVariables.isSmtpAuth());
        assertTrue(SystemVariables.isSmtpStartTlsEnabled());
        assertEquals(SystemVariables.getSmtpHost(), "smtp.mail.com");
        assertEquals(SystemVariables.getSmtpPort(), 587);
    }

    @SuppressWarnings({"unchecked", "static-access"})
    @Test
    public void readConfigurationFromFile_testIOErrors() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
        JsonParserWrapper parser = Mockito.mock(JsonParserWrapper.class);
        Whitebox.setInternalState(configurationReader, "parser", parser);
        Mockito.when(parser.parse(Mockito.any(FileReader.class))).thenThrow(JsonIOException.class);

        boolean hasConfigBeenReadSuccessfully = configurationReader.readConfigurationFromFile();
        assertFalse("Testing file IO error", hasConfigBeenReadSuccessfully);
    }

    @SuppressWarnings({"unchecked", "static-access"})
    @Test
    public void readConfigurationFromFile_testSyntaxErrorRead() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
        JsonParserWrapper parser = Mockito.mock(JsonParserWrapper.class);
        Whitebox.setInternalState(configurationReader, "parser", parser);
        Mockito.when(parser.parse(Mockito.any(FileReader.class))).thenThrow(JsonSyntaxException.class);

        boolean hasConfigBeenReadSuccessfully = configurationReader.readConfigurationFromFile();
        assertFalse("Testing incorrect json syntax in the file.", hasConfigBeenReadSuccessfully);
    }

}
