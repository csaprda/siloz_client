#!/usr/bin/env python
import time, sys, os, pigpio
sys.path.append(os.getcwd())
import DHT22

class SensorReader:
	dataPin=""
	readDellay=""

	def __init__(self, dataPin, readDellay):
		self.dataPin = dataPin
		self.readDellay = readDellay

	def _readSensor(self):
		# this connects to the pigpio daemon which must be started first
		pi = pigpio.pi()
		s = DHT22.sensor(pi, self.dataPin)
		s.trigger()
		time.sleep(self.readDellay)
		reading = '[h]{:3.2f}[h] '.format(s.humidity() / 1.) + '[t]{:3.2f}[t] '.format(s.temperature() / 1.) + 'sensor ['+str(self.dataPin)+']'
		#reading = reading + '{:3.2f}[t]'.format(s.temperature() / 1.)
		#with open("python.log", "a") as myfile:
		#		myfile.write(str(reading))
		return reading

	def _stopSensor(self):
		s.cancel()
		pi.stop()

sensorReader = SensorReader(int(sys.argv[1]),int(sys.argv[2]))
print "readingValue:"+sensorReader._readSensor()
sensorReader._stopSensor
