package ro.csaprda.raspberry;

import ro.csaprda.raspberry.config.ConfigurationReader;
import ro.csaprda.raspberry.scheduler.ReadSensorsTimerTask;
import ro.csaprda.raspberry.scheduler.SchedulerTime;
import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.util.SystemVariables;
import ro.csaprda.raspberry.util.constants.Constant;

import java.util.Timer;
import java.util.logging.Logger;

/**
 * SendReading
 * This class is the starting point of this java application.
 *
 * @author zdeno
 */
public class Main {

    private Logger log = Log.getLog(this.getClass());

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        log.info(Constant.APPLICATION_STARTED);
        if (ConfigurationReader.readConfigurationFromFile()) {
            startTimer();
        }
    }

    /**
     * Start timer to read and send sensor readings. Timer executes the timer
     * task at a fixed rate specified by readInterval
     */
    public void startTimer() {
        ReadSensorsTimerTask readSensorTimerTask = new ReadSensorsTimerTask();
        new Timer()
                .scheduleAtFixedRate(readSensorTimerTask,
                        SchedulerTime.getNextScheduledTimeStamp(),
                        SystemVariables.getReadInterval());
    }

}
