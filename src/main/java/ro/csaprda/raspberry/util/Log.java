package ro.csaprda.raspberry.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Utility class to get a logger that writes anything above INFO (including)
 * level to file. By default, the log file is "application.log"
 * <p>
 * The levels in descending order are:
 * <ul>
 * <li><b>SEVERE</b> (highest value)
 * <li><b>WARNING</b>
 * <li><b>INFO</b>
 * <li>CONFIG
 * <li>FINE
 * <li>FINER
 * <li>FINEST (lowest value)
 * </ul>
 * <p>
 *
 * @author zdeno
 */
public class Log {

    private static Logger log;
    private static FileHandler fileTxt;
    private static SimpleFormatter formatterTxt;
    private static final int TWENTY_MB_IN_BYTES = 20000000;
    private static final int NO_OF_ROUND_ROBIN_LOG_FILES = 5;
    private static final boolean APPEND_LOG_ENTRIES = true;

    private Log() {
    }

    /**
     * Returns a logger for the specified class with the default logging level
     * of INFO. This will give you a logger with a file handler mapped to
     * "application.log" file.
     *
     * @param clazz - The name of the class you want to use Logger in
     * @return Logger
     */
    @SuppressWarnings("rawtypes")
    public static Logger getLog(Class clazz) {
        if (log == null) {
            try {
                fileTxt = new FileHandler(SystemVariables.getLogFile(), TWENTY_MB_IN_BYTES, NO_OF_ROUND_ROBIN_LOG_FILES, APPEND_LOG_ENTRIES);
            } catch (SecurityException | IOException e) {
                e.printStackTrace();
            }
            formatterTxt = new SimpleFormatter();
            fileTxt.setFormatter(formatterTxt);
        }
        log = Logger.getLogger(clazz.getName());
        log.setLevel(Level.INFO);
        log.addHandler(fileTxt);
        return log;
    }
}
