package ro.csaprda.raspberry.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import java.io.FileReader;

/**
 * Wrapper class for the original {@link JsonParser} from google. This class is used because it can be mocked easily, unlike the original class which
 * is
 * declared final.
 *
 * @author zdeno
 */
public class JsonParserWrapper {
    private static final JsonParser PARSER = new JsonParser();

    /**
     * Parses the specified JSON string into a parse tree
     *
     * @param reader JSON text
     * @return a parse tree of {@link JsonElement}s corresponding to the specified JSON
     * @throws JsonParseException if the specified text is not valid JSON
     * @since 1.3
     */
    public JsonElement parse(FileReader reader) {
        return PARSER.parse(reader);
    }

    /**
     * Parses the specified JSON string into a parse tree
     *
     * @param jsonString JSON text
     * @return a parse tree of {@link JsonElement}s corresponding to the specified JSON
     * @throws JsonParseException if the specified text is not valid JSON
     * @since 1.3
     */
    public JsonElement parse(String jsonString) {
        return PARSER.parse(jsonString);
    }

    /**
     * Returns the next value from the JSON stream as a parse tree.
     *
     * @throws JsonParseException if there is an IOException or if the specified
     *                            text is not valid JSON
     * @since 1.6
     */
    public JsonElement parse(JsonReader jsonReader) {
        return PARSER.parse(jsonReader);
    }
}
