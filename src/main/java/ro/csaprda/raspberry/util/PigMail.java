package ro.csaprda.raspberry.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

import static ro.csaprda.raspberry.util.SystemVariables.*;
import static ro.csaprda.raspberry.util.constants.Constant.*;

/**
 * Mailing wrapper class.
 *
 * @author zdeno
 */
public class PigMail {

    /**
     * Sends a mail to all recipients with the provided payload.
     *
     * @param payload - String. Format it with html if you want to make it look
     *                nice.
     */
    public static void sendMail(String payload, String reason) {
        Logger log = Log.getLog(PigMail.class);
        log.info(reason);

        try {
            Transport.send(configureMimeMessage(payload, new MimeMessage(createMailSession(generateSessionProperties()))));
            log.info(LOG_INFO_MAIL_SENT);
        } catch (MessagingException e) {
            log.severe(e.getMessage());
        }
    }

    private static Message configureMimeMessage(String payload, Message mimeMessage) throws MessagingException {
        mimeMessage.setFrom(new InternetAddress(getMailFrom()));
        ArrayList<Address> addresses = new ArrayList<Address>();
        for (String to : getMailTo()) {
            addresses.add(new InternetAddress(to));
        }
        mimeMessage.setRecipients(Message.RecipientType.TO, addresses.toArray(new Address[1]));
        mimeMessage.setSubject(MAIL_SUBJECT);
        mimeMessage.setContent(MAIL_BODY + payload + MAIL_BODY_ENDING, MAIL_FORMAT_INFO);
        return mimeMessage;

    }

    private static Session createMailSession(Properties sessionProperties) {
        Session mailSession = Session.getInstance(sessionProperties, new javax.mail.Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(getMailUsername(), getMailPassword());
            }
        });
        return mailSession;
    }

    private static Properties generateSessionProperties() {
        Properties sessionProperties = new Properties();
        sessionProperties.put(MAIL_SMTP_AUTH, isSmtpAuth());
        sessionProperties.put(MAIL_SMTP_STARTTLS_ENABLE, isSmtpStartTlsEnabled());
        sessionProperties.put(MAIL_SMTP_HOST, getSmtpHost());
        sessionProperties.put(MAIL_SMTP_PORT, getSmtpPort());
        return sessionProperties;
    }

}
