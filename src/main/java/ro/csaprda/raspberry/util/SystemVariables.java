package ro.csaprda.raspberry.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

/**
 * This class keeps values read from the configuration.json file. The values contained in this class are purely system configuration constants. Be
 * careful when editing the content of the configuration.json, wrong values will break the application
 *
 * @author zdeno
 */
public class SystemVariables {
    private static String readingsURL;
    private static String cacheFile = "cache.dat"; // default
    private static String logFile = "application.log"; // default
    private static String mailUsername;
    private static String mailPassword;
    private static String mailFrom;
    private static String smtpHost;
    private static Collection<String> mailTo;
    private static boolean smtpAuth;
    private static boolean smtpStartTlsEnabled;
    private static int smtpPort;
    private static long readInterval;
    private static String configFilePath;
    private static String pythonDir;


    public static String getReadingsURL() {
        return readingsURL;
    }

    public static void setReadingsURL(String readingsURL) {
        SystemVariables.readingsURL = readingsURL;
    }

    public static String getCacheFile() {
        return cacheFile;
    }

    public static void setCacheFile(String cacheFile) {
        SystemVariables.cacheFile = cacheFile;
    }

    public static String getLogFile() {
        return logFile;
    }

    public static void setLogFile(String logFile) {
        SystemVariables.logFile = logFile;
    }

    public static String getMailUsername() {
        return mailUsername;
    }

    public static void setMailUsername(String mailUsername) {
        SystemVariables.mailUsername = mailUsername;
    }

    public static String getMailPassword() {
        return mailPassword;
    }

    public static void setMailPassword(String mailPassword) {
        SystemVariables.mailPassword = mailPassword;
    }

    public static String getMailFrom() {
        return mailFrom;
    }

    public static void setMailFrom(String mailFrom) {
        SystemVariables.mailFrom = mailFrom;
    }

    public static Collection<String> getMailTo() {
        return mailTo;
    }

    public static void setMailTo(Collection<String> mailTo) {
        SystemVariables.mailTo = mailTo;
    }

    public static String getSmtpHost() {
        return smtpHost;
    }

    public static void setSmtpHost(String smtpHost) {
        SystemVariables.smtpHost = smtpHost;
    }

    public static boolean isSmtpAuth() {
        return smtpAuth;
    }

    public static void setSmtpAuth(boolean smtpAuth) {
        SystemVariables.smtpAuth = smtpAuth;
    }

    public static boolean isSmtpStartTlsEnabled() {
        return smtpStartTlsEnabled;
    }

    public static void setSmtpStartTlsEnabled(boolean smtpStartTlsEnabled) {
        SystemVariables.smtpStartTlsEnabled = smtpStartTlsEnabled;
    }

    public static int getSmtpPort() {
        return smtpPort;
    }

    public static void setSmtpPort(int smtpPort) {
        SystemVariables.smtpPort = smtpPort;
    }

    public static long getReadInterval() {
        return readInterval;
    }

    public static void setReadInterval(long readInterval) {
        SystemVariables.readInterval = readInterval;
    }

    public static String getConfigFilePath() {
        if (configFilePath != null) {
            return configFilePath;
        }
        try {
            String path = SystemVariables.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            File file = new File(path);
            if (file.exists() && file.isDirectory()) {
                configFilePath = Files.find(file.toPath(), 5, (path1, basicFileAttributes) -> basicFileAttributes.isRegularFile())
                        .filter(it -> it.endsWith("configuration.json"))
                        .findFirst()
                        .orElse(Paths.get("/tmp/configuration.log"))
                        .toString();
            } else if (file.exists()) {
                configFilePath = file.getParent() + "/configuration.json";
            }
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
        }
        return configFilePath;
    }

    public static String getPythonDir() {
        if (pythonDir != null) {
            return pythonDir;
        }
        try {
            String path = SystemVariables.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            File file = new File(path);
            if (file.exists() && file.isDirectory()) {
                pythonDir = path;
            } else if (file.exists()) {
                pythonDir = file.getParent() + "/";
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return pythonDir;
    }
}
