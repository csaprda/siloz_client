package ro.csaprda.raspberry.util.constants;

/**
 * General constants class
 *
 * @author zdeno
 */
public class Constant {
    public static final String APPLICATION_STARTED = "Application started";
    public static final String MIME_JSON = "application/json";
    public static final String SENDING_MAIL_AT_MIDNIGHT_REASON = "Sending mail because cache is not empty at midnight.";

    // Constants used in the {@link SensorCache}
    public static final String COULD_NOT_READ_CACHE_PREFIX = "Could not read the cache! <br><pre>";
    public static final String COULD_NOT_READ_CACHE_SUFIX = "</pre>";
    public static final String COULD_NOT_READ_FROM_CACHE_REASON = "Sending mail because Could not read cache file.";

    // Constants used in the {@link PigMail}
    public static final String LOG_INFO_MAIL_SENT = "Done sending flying pig mail!";
    public static final String MAIL_BODY_ENDING = "</p></body>";
    public static final String MAIL_FORMAT_INFO = "text/html; charset=utf-8";
    public static final String MAIL_BODY = "<body><p><b>Piggy says:</b> Sensor reading cache is not empty.</p><p><b>Farmer is thinking:</b> Is the server reachable and online?</p><p><b>Lazy dog brings you the the cache:</b>";
    public static final String MAIL_SUBJECT = "Pig Mail oink oink";
    public static final String MAIL_SMTP_PORT = "mail.smtp.port";
    public static final String MAIL_SMTP_HOST = "mail.smtp.host";
    public static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";

    // Constants used in the {@link ReadSensorsTimerTask}
    public static final String LOG_MESSAGE_READ_SENSORS_TIMER_RUNNING = "Running ReadSensors timer event";
    public static final String LOG_MESSAGE_SCRIPT_EXECUTOR_TIMER_RUNNING = "Running ScriptExecutor timer event";

    // Constants used in the {@link SendReading}
    public static final String LOG_MESSAGE_DONE_POSTING_INDIVIDUAL_READINGS = "Done sending readings one by one";
    public static final String LOG_MESSAGE_INDIVIDUAL_READING_POSTED = "Sensor reading: {} - successfuly posted.";
    public static final String LOG_MESSAGE_SEND_INDIVIDUAL_READINGS = "Trying to send readings one by one";
    public static final String LOG_MESSAGE_PUT_FAILED = "Didn't send reading PUT request. Response Line is: ";
    public static final String LOG_MESSAGE_READINGS_POSTED = "PUT request created Successfully";
    public static final String LOG_MESSAGE_ERROR_RESOLVING_HOST_CONNECTION = "Didn't send reading PUT request. There was an error resolving host connection.";

    // Constants used in {@link ConfigurationReader}
    public static final String LOG_MESSAGE_CANNOT_READ_CONFIG_FILE = "Couldn't parse the configuration file! Application cannot continue.";
}
