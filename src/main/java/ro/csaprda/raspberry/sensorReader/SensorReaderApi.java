package ro.csaprda.raspberry.sensorReader;

import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.util.SystemVariables;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Class to allow execution of python sensor reading scripts and retrieving their values
 *
 * @author ivo
 */
public class SensorReaderApi {

    private static final Logger LOG = Log.getLog(SensorReaderApi.class);

    /**
     * Method to run a python script that will read a single sensor reading value
     */
    public String readSensorValues(int giop) {
        try {
            Process process = runScriptProcess(String.valueOf(giop));
            //give the pyton a chance to read the sensor
            try {
                boolean waitFor = process.waitFor(10, TimeUnit.SECONDS);
                if (waitFor)
                    LOG.info(">>> process finished with status: " + process.exitValue());
                else
                    process.destroyForcibly();
            } catch (InterruptedException e) {
                LOG.severe(e.toString());
            }
            checkScriptExecutionForErrors(process);

            return retrieveScriptExecutionResults(process);
        } catch (Exception e) {
            LOG.severe(String.format("Python script [%s] failed to execute", giop));
        }
        return "Error reading sensor";
    }

    private Process runScriptProcess(String giop) throws IOException {
        return new ProcessBuilder("python", SystemVariables.getPythonDir() + "SensorReader.py", giop, SensorReaderScriptsConstants.SMALL_READING_DELLAY).start();
    }

    private String retrieveScriptExecutionResults(Process process) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        return concatenateBufferStream(bufferedReader).toString();
    }

    private void checkScriptExecutionForErrors(Process process) throws IOException {
        if (process.exitValue() != 0) {
            LOG.severe(String.format("Python script failed to execute correctly. Script execution process exited with code: [%s]", process.exitValue()));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            LOG.severe(String.format("Exception thrown while executing python script: %s", concatenateBufferStream(bufferedReader).toString()));
        }
    }

    private StringBuilder concatenateBufferStream(BufferedReader bufferedReader) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String bufferLine;
        while ((bufferLine = bufferedReader.readLine()) != null) {
            stringBuilder.append(bufferLine);
        }
        return stringBuilder;
    }

}
