package ro.csaprda.raspberry.sensorReader;

// ZDENO:No need for interface here, simple class will do. No methods to contract with your class so can easily be converted to a simple constants
// class. Add JavaDoc
public interface SensorReaderScriptsConstants {

    String SMALL_READING_DELLAY = "3";
    // These will have to be mapped out properly
    int SENSOR_1_GIOP_NUMBER = 4;
    int SENSOR_2_GIOP_NUMBER = 17;
    int SENSOR_3_GIOP_NUMBER = 18;
    int SENSOR_4_GIOP_NUMBER = 22;
    int SENSOR_5_GIOP_NUMBER = 23;
    int SENSOR_6_GIOP_NUMBER = 27;
    int SENSOR_7_GIOP_NUMBER = 24;
    int SENSOR_8_GIOP_NUMBER = 4;
    String SENSOR_1_ID = "Sensor 1";
    String SENSOR_2_ID = "Sensor 2";
    String SENSOR_3_ID = "Sensor 3";
    String SENSOR_4_ID = "Sensor 4";
    String SENSOR_5_ID = "Sensor 5";
    String SENSOR_6_ID = "Sensor 6";
    String SENSOR_7_ID = "Sensor 7";
    String SENSOR_8_ID = "Sensor 8";
    double SENSOR_1_HEIGHT = 0.5;
    double SENSOR_2_HEIGHT = 0.5;
    double SENSOR_3_HEIGHT = 1.5;
    double SENSOR_4_HEIGHT = 1.5;
    double SENSOR_5_HEIGHT = 2.5;
    double SENSOR_6_HEIGHT = 2.5;
    double SENSOR_7_HEIGHT = 3.5;
    double SENSOR_8_HEIGHT = 3.5;
}