package ro.csaprda.raspberry.dbService;

import ro.csaprda.raspberry.util.Log;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Logger;

public class SensorReadingDao {

    private static final Logger log = Log.getLog(SensorReadingDao.class);

    private EntityManagerFactory emFactory;
    private EntityManager em;

    public SensorReadingDao(String em) {
        if (emFactory == null) {
            emFactory = Persistence.createEntityManagerFactory(em);
            this.em = emFactory.createEntityManager();
        }
    }

    public void save(SensorReadingEntity sensorReadingEntity) {
        log.info(String.format("Persisting %s", sensorReadingEntity.toString()));
        em.getTransaction().begin();
        em.persist(sensorReadingEntity);
        em.getTransaction().commit();
    }

    public void save(Iterable<SensorReadingEntity> sensorReadingEntities) {
        em.getTransaction().begin();
        for (SensorReadingEntity sensorReadingEntity : sensorReadingEntities) {
            log.info(String.format("Persisting %s", sensorReadingEntity.toString()));
            em.persist(sensorReadingEntity);
        }
        em.getTransaction().commit();
    }
}
