package ro.csaprda.raspberry.dbService;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sensor_reading")
public class SensorReadingEntity {

    public SensorReadingEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "sensor_id")
    private String sensorId;
    private Date date;
    @Column(name = "sensor_height")
    private Double sensorHeight;
    private boolean active;
    private Double temperature;
    private Double humidity;

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getSensorHeight() {
        return sensorHeight;
    }

    public void setSensorHeight(Double sensorHeight) {
        this.sensorHeight = sensorHeight;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public int getId() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (active ? 1231 : 1237);
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((humidity == null) ? 0 : humidity.hashCode());
        result = prime * result + id;
        result = prime * result + ((sensorHeight == null) ? 0 : sensorHeight.hashCode());
        result = prime * result + ((sensorId == null) ? 0 : sensorId.hashCode());
        result = prime * result + ((temperature == null) ? 0 : temperature.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SensorReadingEntity other = (SensorReadingEntity) obj;
        if (active != other.active) {
            return false;
        }
        if (date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!date.equals(other.date)) {
            return false;
        }
        if (humidity == null) {
            if (other.humidity != null) {
                return false;
            }
        } else if (!humidity.equals(other.humidity)) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        if (sensorHeight == null) {
            if (other.sensorHeight != null) {
                return false;
            }
        } else if (!sensorHeight.equals(other.sensorHeight)) {
            return false;
        }
        if (sensorId == null) {
            if (other.sensorId != null) {
                return false;
            }
        } else if (!sensorId.equals(other.sensorId)) {
            return false;
        }
        if (temperature == null) {
            if (other.temperature != null) {
                return false;
            }
        } else if (!temperature.equals(other.temperature)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SensorReading [id=" + id + ", sensorId=" + sensorId + ", date=" + date + ", sensorHeight=" + sensorHeight + ", active=" + active
                + ", temperature=" + temperature + ", humidity=" + humidity + "]";
    }

}
