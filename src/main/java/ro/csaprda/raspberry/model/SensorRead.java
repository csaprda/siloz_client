package ro.csaprda.raspberry.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Object for holding a sensor reading. If you cannot get a reading from a
 * sensor, make those values null, but then change the active state of the
 * sensor to false.
 *
 * @author zdeno
 */
public class SensorRead implements Serializable {
    private static final long serialVersionUID = 1L;

    private String sensorId;
    private Long date;
    private Double sensorHeight;
    private boolean active;
    private Double temperature;
    private Double humidity;

    public SensorRead() {
    }

    /**
     * Parameterized constructor
     *
     * @param sensorId
     * @param date
     * @param sensorHeight
     * @param active
     * @param temperature
     * @param humidity
     */
    public SensorRead(String sensorId, Date date, Double sensorHeight, boolean active, Double temperature, Double humidity) {
        super();
        this.sensorId = sensorId;
        this.date = date.getTime();
        this.sensorHeight = sensorHeight;
        this.active = active;
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public String getSensorId() {
        return sensorId;
    }

    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date.getTime();
    }

    public Double getSensorHeight() {
        return sensorHeight;
    }

    public void setSensorHeight(Double sensorHeight) {
        this.sensorHeight = sensorHeight;
    }

    /**
     * Should be false if the sensor was not able to make a read
     *
     * @return boolean
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Set to true only if the sensor reading for the particular sensor was ok.
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

}
