package ro.csaprda.raspberry.scheduler;

import ro.csaprda.raspberry.util.SystemVariables;

import java.util.Calendar;
import java.util.Date;

/**
 * Scheduler util class for getting the next scheduled date to read sensors.
 *
 * @author zdeno
 */
public class SchedulerTime {

    private static Calendar calendar;
    private static long readIntervalInMiliseconds;
    private static final long HOUR_IN_MILIS = 1000 * 60 * 60;
    private static final long MINUTE_IN_MILIS = 1000 * 60;
    private static final long SECOND_IN_MILIS = 1000;

    /**
     * Generates a date at which the scheduler should trigger the next reading.
     * The date is dependent on the readInterval read from the {@link SystemVariables}.
     *
     * @return Date for the next reading
     */
    public static Date getNextScheduledTimeStamp() {
        readIntervalInMiliseconds = SystemVariables.getReadInterval();
        // take the whole part of each interval only
        int hour = (int) (readIntervalInMiliseconds / HOUR_IN_MILIS);
        int minute = (int) ((readIntervalInMiliseconds - hour * HOUR_IN_MILIS) / (MINUTE_IN_MILIS));
        int second = (int) ((readIntervalInMiliseconds - minute * MINUTE_IN_MILIS - hour * HOUR_IN_MILIS) / SECOND_IN_MILIS);
        calendar = Calendar.getInstance();

        calendar.add(Calendar.HOUR, hour);
        calendar.add(Calendar.MINUTE, minute);
        calendar.add(Calendar.SECOND, second);

        if (minute == 0 && second == 0) { // hours have been set only
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
        } else if (minute > 0 && second == 0) { // minutes have been set only
            int wholeMinute = calendar.get(Calendar.MINUTE) / minute;
            calendar.set(Calendar.MINUTE, wholeMinute * minute);
            calendar.set(Calendar.SECOND, 0);
        } else if (minute == 0 && second > 0) {// seconds have been set only
            int secondsInterval = calendar.get(Calendar.SECOND) / second;
            calendar.set(Calendar.SECOND, secondsInterval * second);
        } else { // both minutes and seconds have been set. We don't care about hours at this point
            int wholeMinute = calendar.get(Calendar.MINUTE) / minute;
            calendar.set(Calendar.MINUTE, wholeMinute * minute);
            int secondsInterval = calendar.get(Calendar.SECOND) / second;
            calendar.set(Calendar.SECOND, secondsInterval * second);
        }

        return calendar.getTime();
    }

}
