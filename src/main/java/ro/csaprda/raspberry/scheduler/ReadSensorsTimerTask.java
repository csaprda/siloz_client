package ro.csaprda.raspberry.scheduler;

import ro.csaprda.raspberry.SensorCache;
import ro.csaprda.raspberry.model.SensorRead;
import ro.csaprda.raspberry.sensorReader.SensorReaderApi;
import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.webService.SensorReadingProcessor;

import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ro.csaprda.raspberry.sensorReader.SensorReaderScriptsConstants.*;
import static ro.csaprda.raspberry.util.constants.Constant.LOG_MESSAGE_READ_SENSORS_TIMER_RUNNING;

/**
 * Class used to read the sensors and send the results to the server. This is a {@link TimerTask} instance and should be used in the context of a
 * scheduler.
 *
 * @author zdeno
 */
public class ReadSensorsTimerTask extends TimerTask {

    private static SensorCache sensorCache;
    private static Logger log = Log.getLog(ReadSensorsTimerTask.class);

    /**
     * Each time you call a timer task in the scheduler, this method is run
     */
    @SuppressWarnings("Duplicates")
    @Override
    public void run() {
        log.info(LOG_MESSAGE_READ_SENSORS_TIMER_RUNNING);
        if (sensorCache == null) {
            sensorCache = SensorCache.getInstance();
        }

        sensorCache.putSensorReading(readSensor(SENSOR_1_GIOP_NUMBER, SENSOR_1_ID, SENSOR_1_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_2_GIOP_NUMBER, SENSOR_2_ID, SENSOR_2_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_3_GIOP_NUMBER, SENSOR_3_ID, SENSOR_3_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_4_GIOP_NUMBER, SENSOR_4_ID, SENSOR_4_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_5_GIOP_NUMBER, SENSOR_5_ID, SENSOR_5_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_6_GIOP_NUMBER, SENSOR_6_ID, SENSOR_6_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_7_GIOP_NUMBER, SENSOR_7_ID, SENSOR_7_HEIGHT));
        sensorCache.putSensorReading(readSensor(SENSOR_8_GIOP_NUMBER, SENSOR_8_ID, SENSOR_8_HEIGHT));

        //SensorReadingProcessor.pushResultsToServer();
        SensorReadingProcessor.saveReadingsLocallyToDb();
    }

    private SensorRead readSensor(int giop, String sensorId, double height) {
        String reading = new SensorReaderApi().readSensorValues(giop);

        log.info(">>>Python returned: " + reading + "]<<<");

        double humidity = 0, temperature = 0;
        humidity = extractValue(reading, humidity, "\\[h\\](.*?)\\[h\\]");
        temperature = extractValue(reading, temperature, "\\[t\\](.*?)\\[t\\]");

        boolean active = true;
        if (temperature > 99 || temperature < -99 || humidity > 100 || humidity < 0)
            active = false;

        return new SensorRead(sensorId, new Date(), height, active, temperature, humidity);
    }

    private double extractValue(String reading, double humidity, String s) {
        Pattern humidityPattern = Pattern.compile(s);
        Matcher humidityMatcher = humidityPattern.matcher(reading);
        while (humidityMatcher.find()) {
            humidity = Double.parseDouble(humidityMatcher.group(1));
        }
        return humidity;
    }

}
