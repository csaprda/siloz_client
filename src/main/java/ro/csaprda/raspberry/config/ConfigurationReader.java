package ro.csaprda.raspberry.config;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import ro.csaprda.raspberry.util.JsonParserWrapper;
import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.util.SystemVariables;
import ro.csaprda.raspberry.util.constants.Constant;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import static ro.csaprda.raspberry.util.SystemVariables.*;

/**
 * Class that reads the application constants from the configuration.json file.
 *
 * @author zdeno
 */
public class ConfigurationReader {

    private static Logger log = Log.getLog(ConfigurationReader.class);
    private static JsonParserWrapper parser = new JsonParserWrapper();

    /**
     * Read the configuration details from the configuration.json file. These application configurations are saved to SystemConfiguration class and
     * accessible from there.
     *
     * @return true if the config was read in successfully, false otherwise. Be aware that if unable to read the config file, the application should
     * not proceed.
     */
    public static boolean readConfigurationFromFile() {
        try {
            log.info("Reading configuration for: " + SystemVariables.getConfigFilePath());
            JsonObject config = (JsonObject) parser.parse(new FileReader(SystemVariables.getConfigFilePath()));
            setCacheFile(config.get("cacheFile").getAsString());
            setLogFile(config.get("logFile").getAsString());
            setReadingsURL(config.get("readingURL").getAsString());

            JsonObject readInterval = (JsonObject) config.get("readInterval");
            int hours = readInterval.get("hours").getAsInt();
            int minutes = readInterval.get("minutes").getAsInt();
            int seconds = readInterval.get("seconds").getAsInt();
            setReadInterval(hours * 60 * 60 * 1000 + minutes * 60 * 1000 + seconds * 1000);

            JsonObject mail = (JsonObject) config.get("mail");
            setMailUsername(mail.get("username").getAsString());
            setMailPassword(mail.get("password").getAsString());
            setMailFrom(mail.get("from").getAsString());
            JsonArray toList = mail.get("to").getAsJsonArray();
            Collection<String> recipients = new ArrayList<String>();
            for (int i = 0; i < toList.size(); i++) {
                recipients.add(toList.get(i).getAsString());
            }
            setMailTo(recipients);

            JsonObject smtp = (JsonObject) mail.get("smtp");
            setSmtpAuth(smtp.get("auth").getAsBoolean());
            setSmtpStartTlsEnabled(smtp.get("starttls").getAsBoolean());
            setSmtpHost(smtp.get("host").getAsString());
            setSmtpPort(smtp.get("port").getAsInt());
        } catch (Exception e) {
            log.log(Level.SEVERE, Constant.LOG_MESSAGE_CANNOT_READ_CONFIG_FILE);
            return false;
        }
        return true;
    }
}
