package ro.csaprda.raspberry;

import ro.csaprda.raspberry.model.SensorRead;
import ro.csaprda.raspberry.scheduler.ReadSensorsTimerTask;
import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.util.PigMail;
import ro.csaprda.raspberry.util.SystemVariables;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static ro.csaprda.raspberry.util.constants.Constant.*;

/**
 * Singleton cache concept. It is called from {@link ReadSensorsTimerTask} at each
 * read. As {@link ReadSensorsTimerTask} itself is a timer task that is generated each
 * hour from the scheduler, it should not get each time a new cache, but the
 * same cache every time.
 * <p>
 * The cache is also persisted to a file, by default cache.dat
 * </p>
 *
 * @author zdeno
 */
public class SensorCache {

    private static Collection<SensorRead> cache;
    private static SensorCache instance;
    private static File cacheFile;

    protected SensorCache() {
    }

    /**
     * Get the instance of the cache.
     *
     * @return - Cache instance or null if cache could not be populated. A mail is sent automatically to your mailing list if cache cannot be
     * populated. The application however can continue in case the cache file is only temporarily not readable.
     */
    public synchronized static SensorCache getInstance() {
        if (instance == null) {
            cacheFile = new File(SystemVariables.getCacheFile());
            instance = new SensorCache();
            cache = new ArrayList<>();
            populateCacheFromFile();
        }
        return instance;
    }

    /**
     * This method needs to be run at the very beginning. It reads the cache
     * from the file and populates it in memory. Afterwards, it is assumed that
     * whatever is in memory is also in the file, so make sure that when you
     * modify the memory cache, you replicate that on the file system as well.
     * <p>
     * A mail is sent to your mailing list if cache could not be populated
     * </p>
     *
     * @return - True if successful, false otherwise
     */
    @SuppressWarnings("unchecked")
    private static boolean populateCacheFromFile() {
        if (!cacheFile.exists()) {
            try {
                return cacheFile.createNewFile();
            } catch (IOException e) {
                Log.getLog(SensorCache.class).severe(e.toString());
                return false;
            }
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cacheFile));) {
            Collection<SensorRead> cacheFromFile = (Collection<SensorRead>) ois.readObject();
            cache.addAll(cacheFromFile);
            return true;
        } catch (EOFException e) {
            // do nothing
        } catch (IOException | ClassNotFoundException e) {
            Log.getLog(SensorCache.class).severe(e.getMessage());
            PigMail.sendMail(COULD_NOT_READ_CACHE_PREFIX + e.getMessage() + COULD_NOT_READ_CACHE_SUFIX,
                    COULD_NOT_READ_FROM_CACHE_REASON);
        }
        return false;
    }

    /**
     * Gets all readings from the cache
     *
     * @return - Collection of {@link SensorRead}
     */
    public synchronized Collection<SensorRead> getAllSensorReadings() {
        return new ArrayList<>(cache);
    }

    /**
     * Removes a collection from the cache.
     *
     * @param sensorReadings Collection to remove
     */
    public synchronized void removeSensorReadings(Collection<SensorRead> sensorReadings) {
        Iterator<SensorRead> iterator = sensorReadings.iterator();
        boolean isRemoved = false;
        while (iterator.hasNext()) {
            cache.remove(iterator.next());
            isRemoved = true;
        }
        if (isRemoved) {
            writeToFile(cache);
        }
    }

    /**
     * Removes a single {@link SensorRead} from the cache
     *
     * @param sensorRead Sensor reading to remove from cache
     */
    public synchronized void removeSensorReading(SensorRead sensorRead) {
        boolean removed = cache.remove(sensorRead);
        if (removed) {
            writeToFile(cache);
        }
    }

    /**
     * Put {@link SensorRead} to cache. Will not <b>replace/update duplicates</b>. Duplicates are highly unlikely due to timestamp of each read.
     * <p>
     * Performs each time a file system write, so <b>expensive operation</b>, use the putSensorReadings() method if possible
     * </p>
     *
     * @param sensorRead - Sensor reading to add to cache
     */
    public synchronized void putSensorReading(SensorRead sensorRead) {
        boolean isAdded = cache.add(sensorRead);
        if (isAdded) {
            writeToFile(cache);
        }
    }

    /**
     * Puts Collection of SensorRead to cache. Will not <b>replace/update</b>
     * duplicates.
     *
     * @param sensorReadings - Collection of sensor readings
     */
    public synchronized void putSensorReadings(Collection<SensorRead> sensorReadings) {
        boolean isAdded = cache.addAll(sensorReadings);
        if (isAdded) {
            writeToFile(sensorReadings);
        }
    }

    /**
     * Writes a collection of sensor readings to file as individual objects.
     * <p>
     * The objects will preserve their ordering.
     *
     * @param sensorReadings - Collection of {@link SensorRead} to write
     * @return - true if successful, false otherwise
     */
    private boolean writeToFile(Collection<SensorRead> sensorReadings) {
        try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(cacheFile))) {
            writer.writeObject(sensorReadings);
            writer.close();
            return true;
        } catch (IOException ex) {
            Log.getLog(this.getClass()).severe(ex.getMessage());
            return false;
        }
    }

}
