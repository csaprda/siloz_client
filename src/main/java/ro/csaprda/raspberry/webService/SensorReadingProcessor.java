package ro.csaprda.raspberry.webService;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import ro.csaprda.raspberry.SensorCache;
import ro.csaprda.raspberry.dbService.SensorReadingDao;
import ro.csaprda.raspberry.dbService.SensorReadingEntity;
import ro.csaprda.raspberry.model.SensorRead;
import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.util.PigMail;
import ro.csaprda.raspberry.util.constants.Constant;

import java.util.*;
import java.util.logging.Logger;

import static ro.csaprda.raspberry.util.constants.Constant.*;

/**
 * Class used to send the reading to the server.
 *
 * @author zdeno
 */
public class SensorReadingProcessor {

    private static SensorCache cache = SensorCache.getInstance();
    private static SensorReadingHttpClient sensorReadingHttpClient = new SensorReadingHttpClient();
    private static final Logger LOG = Log.getLog(SensorReadingProcessor.class);
    private static final SensorReadingDao SENSOR_READING_DAO = new SensorReadingDao("primary");

    public SensorReadingProcessor() {
    }

    public static void saveReadingsLocallyToDb() {
        Collection<SensorRead> sensorReadings = cache.getAllSensorReadings();
        final List<SensorReadingEntity> entities = new ArrayList<>(sensorReadings.size());
        for (SensorRead sensorReading : sensorReadings) {
            SensorReadingEntity entity = getSensorReadingEntity(sensorReading);
            entities.add(entity);
        }
        try {
            SENSOR_READING_DAO.save(entities);
            cache.removeSensorReadings(sensorReadings);
        } catch (Exception e) {
            LOG.severe("Could't persist entities to Database. Exception: " + e.getMessage());
        }
    }

    private static SensorReadingEntity getSensorReadingEntity(SensorRead sensorReading) {
        SensorReadingEntity entity = new SensorReadingEntity();
        entity.setActive(sensorReading.isActive());
        entity.setDate(new Date(sensorReading.getDate()));
        entity.setHumidity(sensorReading.getHumidity());
        entity.setSensorHeight(sensorReading.getSensorHeight());
        entity.setSensorId(sensorReading.getSensorId());
        entity.setTemperature(sensorReading.getTemperature());
        return entity;
    }

    /**
     * Makes a http PUT request to send all cache readings to server. If
     * unsuccessful because readings were malformed, it will try again one at a
     * time. Only successful readings will be removed from cache.
     */
    public static void pushResultsToServer() {

        Collection<SensorRead> sensorReadings = cache.getAllSensorReadings();
        HttpResponse httpResult = sensorReadingHttpClient.sendReadings(sensorReadings);

        logFailedConnectionIfResultIsNull(httpResult);

        if (httpResult.getStatusLine().getStatusCode() == 201) {
            LOG.fine(LOG_MESSAGE_READINGS_POSTED);
            cache.removeSensorReadings(sensorReadings);
        } else {
            LOG.warning(LOG_MESSAGE_PUT_FAILED + httpResult.getStatusLine());
            sendReadingsOneByOne(sensorReadings);
        }
        boolean sendMail = hasMidnightCacheEntries();
        if (sendMail) {
            sendMidnightCacheMail();
        }
    }

    private static void logFailedConnectionIfResultIsNull(HttpResponse httpResult) {
        if (httpResult == null) {
            LOG.warning(LOG_MESSAGE_ERROR_RESOLVING_HOST_CONNECTION);
            return;
        }
    }

    private static void sendReadingsOneByOne(Collection<SensorRead> sensorReadings) {
        LOG.info(LOG_MESSAGE_SEND_INDIVIDUAL_READINGS);

        Iterator<SensorRead> sensorReadingsIterator = sensorReadings.iterator();
        while (sensorReadingsIterator.hasNext()) {
            SensorRead sensorReading = sensorReadingsIterator.next();

            int statusCode = sensorReadingHttpClient.sendReading(sensorReading).getStatusLine().getStatusCode();
            if (statusCode == 201) {
                cache.removeSensorReading(sensorReading);
                //TODO: there's a bug here? keep getting this log but no postings happen.
                LOG.info(LOG_MESSAGE_INDIVIDUAL_READING_POSTED);
            }
        }
        LOG.info(LOG_MESSAGE_DONE_POSTING_INDIVIDUAL_READINGS);
    }

    /**
     * If cache is not empty at midnight after processing midnight readings,
     * then send a mail
     */
    private static void sendMidnightCacheMail() {
        PigMail.sendMail(prepareEmailPayload(),
                Constant.SENDING_MAIL_AT_MIDNIGHT_REASON);
    }

    private static boolean hasMidnightCacheEntries() {
        return cacheHasEntries() && isMidnight();
    }

    private static boolean cacheHasEntries() {
        return cache.getAllSensorReadings().size() != 0 ? true : false;
    }

    private static boolean isMidnight() {
        Calendar calendar = Calendar.getInstance();
        int hourPlusMin = calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE);
        return hourPlusMin == 0 ? true : false;
    }

    private static String prepareEmailPayload() {
        String payload = "";
        Iterator<SensorRead> it = cache.getAllSensorReadings().iterator();
        while (it.hasNext()) {
            SensorRead sensorRead = it.next();
            payload += "<p>";
            payload += new Gson().toJson(sensorRead);
            payload += "</p>";
        }
        return payload;
    }

}
