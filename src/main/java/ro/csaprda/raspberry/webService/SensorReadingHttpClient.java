package ro.csaprda.raspberry.webService;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import ro.csaprda.raspberry.model.SensorRead;
import ro.csaprda.raspberry.util.Log;
import ro.csaprda.raspberry.util.constants.Constant;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.logging.Logger;

import static ro.csaprda.raspberry.util.SystemVariables.getReadingsURL;

/**
 * This class is used for sending {@link SensorRead} HTTP requests.
 *
 * @author zdeno
 */
public class SensorReadingHttpClient {

    private Logger log;

    public SensorReadingHttpClient() {
        log = Log.getLog(this.getClass());
    }

    /**
     * Send a PUT request to server that puts the provided Collection of objects
     * as payload.
     *
     * @param readings - Collection of {@link SensorRead} objects to send
     * @return - {@link HttpResponse}
     */
    public HttpResponse sendReadings(Collection<SensorRead> readings) {
        String json = new Gson().toJson(readings);
        return sendPutRequest(json, getReadingsURL());
    }

    /**
     * Send a PUT request to server that puts the provided sensor reading as payload.
     *
     * @param reading - {@link SensorRead} to send
     * @return - {@link HttpResponse}
     */
    public HttpResponse sendReading(SensorRead reading) {
        String json = new Gson().toJson(reading);
        return sendPutRequest(json, getReadingsURL() + reading.getSensorId());
    }

    private HttpResponse sendPutRequest(String payload, String url) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPutRequest = new HttpPut(url);

        StringEntity stringEntity;
        try {
            stringEntity = new StringEntity(payload);
            stringEntity.setContentType(Constant.MIME_JSON);
            httpPutRequest.setEntity(stringEntity);
            return httpClient.execute(httpPutRequest);
        } catch (UnsupportedEncodingException e) {
            log.warning(e.getMessage());
        } catch (ClientProtocolException e) {
            log.warning(e.getMessage());
        } catch (IOException e) {
            log.warning(e.getMessage());
        }
        return null;
    }
}
