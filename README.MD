in order to run this do:

* make sure default java compiler is java 8

>>> sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk-8-oracle-arm-vfp-hflt/bin/javac" 2000

		* the "2000" is priority and if highes will make it auto

*to check/manually select version 

>>> sudo update-alternatives --config javac

		* you should see something like:
============================================================================================		
There are 3 choices for the alternative javac (providing /usr/bin/javac).

  Selection    Path                                              Priority   Status
------------------------------------------------------------
  0            /usr/lib/jvm/jdk-8-oracle-arm-vfp-hflt/bin/javac   2000      auto mode
  1            /usr/lib/jvm/java-6-openjdk-armhf/bin/javac        1057      manual mode
  2            /usr/lib/jvm/jdk-7-oracle-armhf/bin/javac          317       manual mode
* 3            /usr/lib/jvm/jdk-8-oracle-arm-vfp-hflt/bin/javac   2000      manual mode

Press enter to keep the current choice[*], or type selection number: 3
============================================================================================

*also make sure your default jdk is jdk8

>>> sudo update-alternatives --config java

		* you should see something like:
============================================================================================
There are 3 choices for the alternative java (providing /usr/bin/java).

  Selection    Path                                                 Priority   Status
------------------------------------------------------------
  0            /usr/lib/jvm/java-6-openjdk-armhf/jre/bin/java        1057      auto mode
  1            /usr/lib/jvm/java-6-openjdk-armhf/jre/bin/java        1057      manual mode
  2            /usr/lib/jvm/jdk-7-oracle-armhf/jre/bin/java          317       manual mode
* 3            /usr/lib/jvm/jdk-8-oracle-arm-vfp-hflt/jre/bin/java   318       manual mode

Press enter to keep the current choice[*], or type selection number: 
============================================================================================= 
http://crunchify.com/how-to-create-build-java-project-including-all-dependencies-using-maven-maven-resources-maven-dependency-maven-jar-plugin-tutorial/

http://stackoverflow.com/questions/941754/how-to-get-a-path-to-a-resource-in-a-java-jar-file

* package the jar

>>> mvn clean install

* then run it

>>> cd target/jar/
>>> java -jar Client.jar

* Create your startup script for pi:
>>> nano /etc/rc.local
#Start the silo application at startup
sudo pigpiod
su pi -c 'echo `whoami` user started pigpiod at `date` >> ~/startup.log'
su pi -c 'cd /home/pi/gitRepo/siloz_client/ && mvn clean install -DskipTests=true'
su pi -c 'java -jar /home/pi/gitRepo/siloz_client/target/jar/Client.jar &'
su pi -c 'echo `whoami` user started client (java -jar /home/pi/gitRepo/siloz_client/target/Client.jar) at `date` >> ~/sta$
su pi -c 'java -jar /home/pi/Server/spring-boot-silo-monitor-1.0-SNAPSHOT.war &'
su pi -c 'echo `whoami` user started server (java -jar /home/pi/Server/spring-boot-silo-monitor-1.0-SNAPSHOT.war) at `date$
exit 0
